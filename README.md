# RPI comme VPN

## Installation

https://www.raspberrypi.org/software/

 - Télécharger le logiciel **imager_1.5.exe**
 - Lors de l'installation de l'image sur la carte, choisir une distribution **lightweight**, **sans desktop** et sans **logiciels pré-installés**
 - Démarrer le RPI avec la carte
 - Lors de la première connexion, rien n'est activé, il faut brancher un écran et un clavier
 - Pour activer le SSH lors du premier démarrage, il suffit de créer un fichier "ssh" à la racine de la carte SD avant de la plugguer dans le RPI

## Connexion et configuration

 - Se connecter avec l'utilisateur **pi**, le mot de passe est **raspberry**
 - Commencer par activer le clavier **FR (AZERTY)** en lançant la commande de configuration normalement affichée à l'écran "`sudo raspi-config`"
 - Changer la TimeZone: Europe/Paris, toujours depuis le **raspi-config**
 - Changer le mot de passe de l'utilisateur **pi** avec la commande `sudo passwd`
 - Changer aussi le mot de passe **root** avec la commande `sudo passwd root`
 - Mises à jour: `apt update` / `apt upgrade`
 - Installer vim: `apt install vim`
 - Redémarrer une première fois: `sudo shutdown -r now`
 - Se connecter avec l'utilisateur **root**
 - Editer la configuration SSH en interdisant les connexions SSH **root** et en changeant le **port par défaut**:
	 - `vim /etc/ssh/sshd_config`
	 - `Port 7777`
	 - `PermitRootLogin no`
 - Reload SSH et activer le démarrage automatique de SSH au boot du système
	 - `sudo /etc/init.d/ssh reload`
	 - `sudo systemctl enable ssh`
 - Puis redémarrer le RPI: `sudo shutdown -r now`

## Autre utilisateur

On va créer un autre utilisateur et supprimer celui par défaut. Pour faciliter la compréhension de la suite de cette documentation, on choisira l'utilisateur **ruiadr**.

 - On commence par se connecter en SSH avec l'utilisateur **pi**
 - On lance la commande `sudo adduser ruiadr`
 - On se déconnecte et on se reconnecte avec l'utilisateur nouvellement créé
 - Depuis le nouvel utilisateur, on se log à l'utilisateur root: `su root`
 - On supprime l'utilisateur **pi** par défaut: `sudo userdel -r pi`
	 - L'option **-r** permet de supprimer le répertoire **home** et le **mail spool**
	 - Si des process sont ouverts par cet utilisateur, les tuer: `kill -9 <PID>`

## Sécurisation

### fail2ban SSH

On va installer **fail2ban** pour bloquer temporairement les tentatives de connexion infructueuses.

```
> sudo apt install -y iptables fail2ban

# Ajout configuration personnalisée pour SSH
> vim /etc/fail2ban/jail.d/defaults-debian.conf

# Configuration SSH à ajouter au fichier
[sshd]
enabled = true
maxretry = 5
findtime = 600
banaction = iptables-multiport
bantime = 600
ignoreip = 127.0.0.1/8

# On redémarre fail2ban et on s'assure que les règles SSH
# soient correctement activées

> fail2ban-client restart
> fail2ban-client status
```

### iptables

Avec **iptables** précédemment installé, on va bloquer toutes les entrées sauf celle qui se trouve sur le port **SSH**. Dans la suite de la procédure, bien veiller à saisir le bon port **SSH**, surtout s'il est différent de celui indiqué plus haut dans la documentation.

```
# Reset des règles
> sudo iptables -F

# Général, nécessaire pour garder les connexions ouvertes et autoriser les communications locales
> sudo iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
> sudo iptables -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
> sudo iptables -A INPUT -i lo -j ACCEPT
> sudo iptables -A OUTPUT -o lo -j ACCEPT

# Autorisation du port 7777
> sudo iptables -A INPUT -p tcp --dport 7777 -m iprange --src-range 192.168.1.1-192.168.1.253 -j ACCEPT

# Autorisation du port 80 (à adapter au besoin), sera nécessaire plus tard dans la procédure
> sudo iptables -A INPUT -p tcp --dport 80 -m iprange --src-range 192.168.1.1-192.168.1.253 -j ACCEPT

# Interdiction de tout le reste
> sudo iptables -A INPUT -j DROP

# Affichage des règles enregistrées
> sudo iptables -L -v -n
```
Ensuite il nous faut rendre ces règles persistantes afin qu'elles soient réappliquer à chaque démarrage du RPI.

```
> apt install iptables-persistent
```
Durant l'installation, le logiciel demande s'il faut sauvegarder les règles actuellement utilisées, il suffit de répondre **Yes**.

On peut redémarrer le **rpi** pour s'assurer que tout soit correctement en place: `sudo shutdown -r now`.

Le RPI est prêt à être utilisé, on peut réaliser un petit nettoyage: `apt clean && apt autoremove --purge`

## Bloquer les IPv6

On désactive ipv6.

```
> sudo vim /etc/sysctl.conf

# A ajouter en fin du fichier.

net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1

> sudo sysctl -p

```

Mode parano: on ajoute une couche avec ip6tables et on persiste le tout!

```
> sudo ip6tables -A INPUT -j DROP
> sudo ip6tables -A OUTPUT -j DROP
> sudo ip6tables -A FORWARD -j DROP

> sudo ip6tables-save  > /etc/iptables/rules.v6

> sudo ip6tables -L
```

Derniers checks:

```
> [ -f /proc/net/if_inet6 ] && echo 'IPv6 OK' || echo 'No IPv6'
> lsmod | grep -qw ipv6 && echo "IPv6 kernel driver loaded and configured." || echo "IPv6 not configured and/or driver loaded on the system."
```

- Commande 1: **IPv6 OK** ou **No IPv6**
- Commande 2: **IPv6 not configured and/or driver loaded on the system.**

## Installer & configurer le VPN

Pour cela on va utiliser **PiVPN** pour installer & configurer **OpenVPN**.

`curl -L https://install.pivpn.io | bash`

Une fois la commande lancée, suivre la procédure affichée dans l'interface de configuration.

Points clés à connaître avant de se lancer:
 - L'adresse IP locale du RPI doit **être statique** (configuration à réaliser sur la box)
	 - Différent d'une box à l'autre, mais se référer à la section DHCP de la configuration avancée
 - Sur cette même box il faudra mettre en place une redirection de port **1194 externe vers 1194 interne du RPI via IP statique**
 - Durant la phase d'installation, l'utilitaire va **automatiquement ajouter les règles** dans **iptables** pour autoriser les différentes communications, puis les persister avec **iptables-persistent**
 - **DNS Provider**: choisir **OpenDNS**
 - Si l'adresse IP de la connexion internet n'est pas fixe, possibilité d'utiliser un **DynDNS** avec un NDD sur OVH par exemple. L'objectif étant d'utiliser un nom de domaine plutôt que l'adresse IP internet (donc dynamique) en cours d'utilisation durant la phase de configuration
	 - Choisir **DNS Entry** plutôt que **Public IP** et saisir le nom de domaine utilisé pour le VPN. Pour la configuration côté OVH, plus d'informations ici: https://docs.ovh.com/fr/domains/utilisation-dynhost/
	 - Concernant la configuration sur la box, tout dépend des FAI, mais se trouve généralement dans la configuration avancée, section "**DynHost**"
	 - Les certificats seront stockés dans le home de l'utilisateur **ruiadr**, point important à noter pour la suite de la procédure

Le procédure est relativement simple à comprendre, ci-dessous, quelques liens la détaillant:
 - https://korben.info/pivpn-transformer-raspberry-pi-serveur-openvpn.html
 - https://medium.com/@piratelab.io/pivpn-installer-un-vpn-maison-avec-un-raspberry-pi-e71ba47f3e33

Une fois terminé, puis les différents certificats & comptes créés, il suffit de tester avec un client OpenVPN.

https://openvpn.net/download-open-vpn/

### Bugs rencontrés

Durant la création de comptes & certificats l'absence du répertoire `/etc/openvpn/ccd` a provoqué des erreurs. Il suffit de créer manuellement ce répertoire.

## Apache & PHP pour mettre à disposition les certificats

Dans la continuité des éléments mis en place, on souhaite mettre au point une interface permettant aux utilisateurs du VPN de récupérer leurs certificats, à condition qu'ils soient connectés au réseau local, et disposent du même masque de sous-réseau (cf la configuration de iptables mis en place précédemment).

On utilisera Apache et PHP.

On prépare l'hébergement des fichiers de l'interface web avec l'utilisateur **ruiadr**.

L'applicatif web dispose de son propre repository disponible ici: https://gitlab.com/ruiadr/rpivpnweb. Le fichier **README.MD** détaille la procédure à suivre.

Le sources devront être hébergées directement sous `/home/ruiadr/web/` (le **DocumentRoot**).

Enfin, avec l'utilisateur **root** cette fois-ci, on change la page Apache par défaut par la notre. Le RPI ne servant que de **VPN**, **Apache** n'aura pas d'autres fonctions que celle que nous sommes entrain de mettre en place ici, donc pas nécessaire de créer un **VirtualHost** dédié.

```
# Installation de PHP et implicitement de Apache
> apt install php
> mv /var/www/html /var/www/html.orig
> ln -s /home/ruiadr/web/ /var/www/html
```

Il est ensuite possible d'accéder au RPI depuis une connexion au réseau local (saisir l'adresse IP du RPI dans un navigateur).

L'interface affiche la liste des certificats disponibles, ainsi que les URLs associées qui peuvent être récupérées pour le client **OpenVPN** de l'ordinateur à connecter.

Chaque certificat de connexion au VPN doit être généré avec un mot de passe qu'il faudra stocker dans un gestionnaire de mot de passe (ex: Dashlane) avant de les communiquer à la personne concernée.

## Révoquer ou ajouter un certificat

```
pivpn revoke xxx
pivpn add
```
